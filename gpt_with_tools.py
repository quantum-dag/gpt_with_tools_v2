import os
import openai
import argparse
import re

# Set up OpenAI parameters
openai.api_key = os.environ["MY_GPT_KEY"]
openai.organization = os.environ["MY_GPT_ORG"]
model="gpt-4"
temperature=0.7

# General Tool class
from Tool import Tool, msg_list
Tools:list[Tool] = []

# Import tool classes
from Calculator import Calculator
from CitationChecker import CitationChecker
Tool_Classes = [ Calculator, CitationChecker ]

def gpt(messages: msg_list, temperature:float)->str:
    response = openai.ChatCompletion.create(model="gpt-4", messages=messages, temperature=temperature)
    return str(getattr(response, 'choices')[0].message["content"]).strip()

def main(debug=False):
    # Initial system message
    messages = [ 
        {'role':'system', 'content':
            'You are ChatGPT, a large language model trained by OpenAI. ' 
            'Answer as concisely as possible.'
        },
    ]

    # Create the tool objects
    for tc in Tool_Classes:
        tool = tc()
        Tools.append(tool)
        messages += tool.instructions
        tool.debug = debug

    if debug:
        print('***** Intruction Messages *****')
        for m in messages:
            print(f"{m['role'].capitalize()}: {m['content']}\n")
        print('***** End Instructions Messsages *****')        

    print('When prompted, you may type a question, or exit to end the session,\n'
          'or +d to turn on debugging, or -d to turn off debugging.')

    while True:
        # Get user input
        question = input('User: ').strip()
        # Handle special user commands
        if re.match('exit', question, flags=re.IGNORECASE):
            break
        elif question.startswith('+d'):
            debug = True
            continue
        elif question.startswith('-d'):
            debug=False
            continue
        
        messages.append({'role':'user', 'content':question})
        # Get GPT response and handle tool requests
        while True:
            reply = gpt(messages, temperature)
            m = None
            t = Tools[0]
            for t in Tools:
                m = re.search(t.pattern, reply)
                if m:
                    break
            if not m:
                break
            # Handle tool request
            if debug:
                print(f"Captured command {reply}")
                print(f"Invoked tool {t.name}")
            value = t.run(m)
            if debug:
                print(f"Tool returns {value}")
            messages.append({'role':'user', 'content':value})
        # Print response for non-tool output
        print(f"Assistant: {reply}")
    print("Ending session, goodbye.")

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="Augment ChatGPT with tools")
    parser.add_argument('--debug', '-d', action='store_true', help='Print debugging info')
    args = parser.parse_args()
    main(debug=args.debug)